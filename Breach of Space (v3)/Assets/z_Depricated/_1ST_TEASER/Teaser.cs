using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teaser : MonoBehaviour
{
    public Animator mainRobotAnim;
    public Animator secondRobotAnim;

    public Animator thirdRobotAnim; // might not need?

    public List<Light> allLights;

    int shotNum = 0;
    int liteNum = 0;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void TurnOffTheLights()
    {
        if(liteNum <= allLights.Count)
        {
            allLights[liteNum].color = Color.red;
            liteNum++;
        }
    }

    public void NextShot()
    {
        switch (shotNum)
        {
            case 0:
                secondRobotAnim.SetBool("Dead", true);
                shotNum++;
                break;
            case 1:
                mainRobotAnim.SetBool("Waving", true);
                shotNum++;
                break;
            case 2:
                mainRobotAnim.SetBool("Dead", true);
                shotNum++;
                break;
            default:
                break;
        }
    }

}
