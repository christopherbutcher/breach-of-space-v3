using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public static int failed;
    public static float chaos;

    public static FMOD.Studio.EventInstance gameMusic;

    // Start is called before the first frame update
    void Start()
    {
        gameMusic = FMODUnity.RuntimeManager.CreateInstance("event:/m_GreetingsFromNull");
        gameMusic.start();
        chaos = 0;
        failed = 0;
        gameMusic.setParameterByName("LevelCompleted", 1f); // This should be zero in the future!
        //Invoke("SetParameters", 0.25f);
    }

    private void Update()
    {
        SetParameters();
    }

    static void SetParameters()
    {
        gameMusic.setParameterByName("Chaos", chaos);
        gameMusic.setParameterByName("LevelFailed", (float)failed);
    }

    /// <summary>
    /// When player loses or the scene is reloaded,
    ///     this must be called in order to not
    ///     have multiple songs playing in scene.
    /// </summary>
    public static void FadeOutLevelMusic()
    {
        gameMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public static void GameOver(bool win)
    {
        if (!win)
        {
            failed = 1;
        }
        else
        {
            failed = 0;
        }
        SetParameters();
    }

    public static void PlaySound(string path, Vector3 pos)
    {
        //Debug.Log("PATH: " + path);
        FMODUnity.RuntimeManager.PlayOneShot(path, pos);
    }
}
