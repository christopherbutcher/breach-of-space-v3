using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 2021.09.30 @ 07:43 AM - CB.
public abstract class Waypoint : MonoBehaviour
{
    public abstract IEnumerator WaypointBehavior(Robot thisRobot);
    public void StopCurrentObjective(Robot thisRobot)
    {
        Debug.Log(thisRobot + " has stopped all Coroutines!");
        FMODUnity.StudioEventEmitter emitter;
        TryGetComponent<FMODUnity.StudioEventEmitter>(out emitter);
        if(emitter != null)
        {
            emitter.Stop();
        }
        StopCoroutine(WaypointBehavior(thisRobot));
        thisRobot.SetRobotStatus(Robot.RobotStates.Idle);
        //thisRobot.busy = false;
    }
}
