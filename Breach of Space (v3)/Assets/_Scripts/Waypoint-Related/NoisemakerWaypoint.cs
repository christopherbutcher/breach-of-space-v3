using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 2021.09.20 @ 10:18 PM - CB.
public class NoisemakerWaypoint : Waypoint
{
    public override IEnumerator WaypointBehavior(Robot thisRobot)
    {
        // HELLO.
        // HERE WILL BE EVERYTHING THE ROBOT DOES WHEN IT GET TO THE NOISEMAKER,
        // GIVEN THAT THE NOISEMAKER DOESN'T TURN ITSELF OFF BEFORE IT GETS THERE.
        // IF SO, THEN REMOVE THIS FROM THE DICTIONARY IN ROBOTBLACKBOARD.
        // ALSO, THIS SHOULD BE ATTACHED TO THE NOISEMAKER PREFAB.
        // THANKS.
        yield return null;
    }
}
