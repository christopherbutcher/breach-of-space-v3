using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneTimer : MonoBehaviour
{
    [SerializeField]
    private float delayBeforeLoading = 10f;
    [SerializeField]
    private string sceneNameToLoad;

    private float timeElasped;

    private void Update()

    {
        timeElasped += Time.deltaTime;

        if (timeElasped > delayBeforeLoading)
        {
            AudioHandler.FadeOutLevelMusic();
            SceneManager.LoadScene(sceneNameToLoad);
        }
    }
}
