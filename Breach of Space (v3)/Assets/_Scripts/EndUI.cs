using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndUI : MonoBehaviour
{
    public GameObject menu;
    public GameObject gO;
    //public ChaosBar chaos;

    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(false);
        //gO.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(menu.activeInHierarchy == true || gO.activeInHierarchy == true)
        {
            PauseGame();
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            UnpauseGame();
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void QuitGame()
    {
        Debug.LogError("QUIT!!!");
        Application.Quit();
    }

    public void ShowMenu()
    {
        menu.SetActive(true);
    }

    public void GameOver()
    {
        gO.SetActive(true);
        AudioHandler.GameOver(false);
    }

    public void ReloadScene()
    {
        //This should reload to the start of the game or the start menu.
        AudioHandler.FadeOutLevelMusic();
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
    }

}
