using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sabotage : MonoBehaviour
{
    public bool boomer = false;
    public GameObject partParent;
    public VentBoom vent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AttemptSabo()
    {
        GetComponent<SabotageMinigame>().SpawnGame();
    }

    public void Break()
    {
        print("break called");
        RobotBlackboard.TurnOnSabotage(this.GetComponent<SabotageWaypoint>());
        foreach(Transform child in partParent.transform)
        {
            child.gameObject.GetComponent<ParticleSystem>().Play();
        }
        if (AudioHandler.chaos < 0.36f)
        {
            AudioHandler.chaos = 0.36f;
        }
        if (boomer)
        {
            vent.Explode();
        }
        
    }
}
