using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public int level;
    
    public void SavePlayer()
    {
        SaveSystem.SavePlayerData(this);
    }

    public void LoadPlayer()
    {
        SaveData sd = SaveSystem.LoadSaveData();
        level = sd.level;
    }
}
