using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCamera : MonoBehaviour
{
    /// <summary>
    /// 
    /// CURRENTLY A COPY OF VIEWFINDER.
    /// MIGHT USE LATER?
    /// </summary>
    public Vector3 currentRoom;
    public GameObject currentCharacter;
    public GameObject currentInteractable;

    Camera mainCamera;
    Animator camAnimator;

    bool viewing_Character;
    bool viewing_Room;

    public CinemachineStateDrivenCamera stateCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
