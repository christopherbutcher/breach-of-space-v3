using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Panda;

public class RobotPatrol : RobotMovement
{
    /*
    /// <summary>
    /// keeps track of all spots where the Robot wants to patrol.
    /// It should be an ORDERED array of transforms attached to empty GameObjects.
    /// </summary>
    public Transform[] myPatrolPositions;

    /// <summary>
    /// The index of the currently selected Patrol Position.
    /// </summary>
    int currentPatrolPosition;

    /// <summary>
    /// Reference to the player.
    /// Used to determine the direction when Raycasting
    ///  to look for them.
    /// </summary>
    public GameObject playerObject;

    /// <summary>
    /// The last place the player has been seen.
    /// </summary>
    Vector3 lastPlayerPosition;

    /// <summary>
    /// The area around the Robot that the Robot
    ///     can see/hear.
    /// </summary>
    SphereCollider observationRange;

    public override void Start()
    {
        base.Start();
        // Don't know if the player is going to be an entity or be tagged this...
        // But this'll work for now.
        playerObject = GameObject.FindWithTag("Player");
    }

    public override void Update()
    {
        base.Update();

        // for now, just go between the two spots.
        if (!walking && !working) 
        {
            StartCoroutine(LookAround());
        }
        // If we can see the player, get 'em.
        //if (LineofSight()) StopCoroutine(LookAround()) ;
    }
    
    /// <summary>
    /// Get the next Patrol Position and set it as the destination.
    /// </summary>
    public void SetNextRobotDestination() 
    {
            // Just cycle through the patrol positions.
            // We'll assume that, for now, if the robot is disturbed on its path...
            // It'll just go to its next position anyways.
        currentPatrolPosition = (currentPatrolPosition + 1) % myPatrolPositions.Length; 

        // Get its position...
        Vector3 nextPositionVector = myPatrolPositions[currentPatrolPosition].position;

        // And feed it into the base class' SetRobotDestination.
        SetRobotDestination(nextPositionVector);
    }

    /*public void SetRobotDestination(Vector3 target) 
    {
        // Just cycle through the patrol positions.
        // We'll assume that, for now, if the robot is disturbed on its path...
        // It'll just go to its next position anyways.
        // Get its position...
        Vector3 nextPositionVector = target;
        // And feed it into the base class' SetRobotDestination.
        SetRobotDestination(nextPositionVector);
    }*/ /*

    /// <summary>
    /// Called when we arrive at our "destination." 
    /// Doesn't make the robot actually look around for now, but it will.
    /// When it's done, get the next destination.
    /// </summary>
    IEnumerator LookAround() 
    {
        // Set him to be working. This is just a visual indicator for now

        working = false;
        
    = RobotStates.Searching;
        // For now, just wait five seconds.
        // You could also just have the robot rotate to look around briefly here.

        yield return new WaitForSeconds(5f);
        working = false;

        SetNextRobotDestination();
    }

    /// <summary>
    /// Can this robot see the player from their current position?
    /// Not perfect -- only looks at one line straight from our position to the player, so it could be buggy.
    /// Taken directly from the OH codebase.
    /// </summary>
    /// <returns></returns>
    private bool LineofSight() // should this just return a bool & let the Coroutine start elsewhere???? -c.
    {
        RaycastHit hit;
        // Find the direction of the player relative to us
        var rayDirection = playerObject.transform.position - transform.position;

        if (Physics.Raycast(transform.position, rayDirection, out hit, 3f))
            // NOTE: Mathf.Infinity is used here, so the robots have infinite line of sight distance
            //      HEY, I changed this! Now it's 3f! Hope that's ok! Infinite sight is bad for player! -c.
        {
            if (hit.transform.tag == "Player")
            {
                // We see the player -- as long as this is true, keep updating.
                lastPlayerPosition = hit.transform.position;
                // Chase them down -- Dunno if this is what y'all want but here it is
                //SetRobotDestination(playerPosition);
                return true;
            }
            else
            {
                //StartCoroutine(LookAround()); <--- this should be somewhere else -c.
                return false;
            }
        }
        else
        {
            //StartCoroutine(LookAround()); <--- this should be somewhere else -c.
            return false;
        }
    }

    void PlaySound(string path)
    {
        //Debug.Log("PATH: " + path);
        FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
    }

    /// <summary>
    /// Not final, but this is an example of Task structure for BTs. -c.
    /// </summary>
    [Task]
    public void PlayerSeen()
    {
        if(Vector3.Distance(playerObject.transform.position, this.transform.position) < 1.5f)
            //&& ( || ?) LINE OF SIGHT STILL.
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
        }
    }

    [Task]
    public void PursuePlayer()
    {
        if(Vector3.Distance(playerObject.transform.position, this.transform.position) < 5f)
            //&& LINE OF SIGHT STILL.
        {
            Task.current.Succeed();
        }
        else
        {
            Task.current.Fail();
            // Start looking around again before heading back to work.
        }
    }

    [Task]
    void GoToNextWaypoint()
    {
        if (myPatrolPositions.Length <= 1)
        {
            Task.current.Succeed();
        }
        else
        {
            if(this.curState == RobotStates.Idle || this.curState == RobotStates.Walking)
            {
                SetNextRobotDestination();
                Task.current.Succeed();
            }
            else
            {
                Task.current.Fail();
            }
            // IF NOT IN PURSUIT OF PLAYER
            // IF NOT INVESTIGATING
            // IF NOT SUSPICIOUS
            // IF NOT FIXING
            // IF NOT ANYTHING
            // GO TO NEXT WAYPOINT!
            // SUCCEED!!!
        }
    }*/
}
