using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class POV : MonoBehaviour
{
    /*
    public List<GameObject> aCamChannels;
    public List<GameObject> bCamChannels;
    public List<GameObject> cCamChannels;
    public List<GameObject> dCamChannels;

    public static Camera mainCamera;

    static Camera currentCamera;
    static GameObject currentScreen;

    static RobotMovement currentRobot;

    public enum CameraView {A,B,C,D,Z};

    CameraView cameraView = new CameraView();

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = this.GetComponent<Camera>();
        currentCamera = null;
        cameraView = CameraView.Z;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit mainHit;
            Ray mainRay = mainCamera.ScreenPointToRay(Input.mousePosition);

            // Play click sound
            if (currentCamera == null)
            {
                if(Physics.Raycast(mainRay, out mainHit))
                {
                    if (aCamChannels.Contains(mainHit.collider.gameObject))
                    {
                        currentCamera = mainHit.collider.GetComponent<ScreenScript>().thisCamera;
                        cameraView = CameraView.A;
                        ChangeView(cameraView);
                    }
                    else if (bCamChannels.Contains(mainHit.collider.gameObject))
                    {
                        currentCamera = mainHit.collider.GetComponent<ScreenScript>().thisCamera;
                        cameraView = CameraView.B;
                        ChangeView(cameraView);
                    }
                    else if (cCamChannels.Contains(mainHit.collider.gameObject))
                    {
                        currentCamera = mainHit.collider.GetComponent<ScreenScript>().thisCamera;
                        cameraView = CameraView.C;
                        ChangeView(cameraView);
                    }
                    else if (dCamChannels.Contains(mainHit.collider.gameObject))
                    {
                        currentCamera = mainHit.collider.GetComponent<ScreenScript>().thisCamera;
                        cameraView = CameraView.D;
                        ChangeView(cameraView);
                    }
                    else
                    {
                        // Not a screen, or something isn't set up right.
                    }
                }
            }
            else
            {
                if(Physics.Raycast(mainRay, out mainHit))
                {
                    if(currentRobot == null)
                    {
                        Ray outRay = currentCamera.ViewportPointToRay(mainHit.textureCoord);
                        RaycastHit[] xRay = Physics.RaycastAll(outRay);

                        foreach(RaycastHit h in xRay)
                        {
                            if(h.collider.gameObject.tag == "Robot")
                            {
                                currentRobot = h.collider.GetComponent<RobotMovement>();
                                //currentRobot.selected = true;
                                // Find a way to keep other Robots from being selected (Array?).
                            }
                            else if(h.collider.gameObject.tag == "Bounds")
                            {
                                // stop searching.
                            }
                            else
                            {
                                // keep searching.
                            }
                        }
                    }
                    else
                    {
                        Ray outRay = currentCamera.ViewportPointToRay(mainHit.textureCoord);
                        RaycastHit outHit;

                        if(Physics.Raycast(outRay, out outHit))
                        {
                            Debug.Log(outHit.collider.name);
                            switch (outHit.collider.tag)
                            {
                                case "Robot":
                                    //currentRobot.selected = false;
                                    currentRobot = outHit.collider.GetComponent<RobotMovement>();
                                    //currentRobot.selected = true;
                                    break;
                                case "Job":
                                    break;
                                case "Item":
                                    break;
                                default:
                                    currentRobot.destination = outHit.point;
                                    currentRobot = null;
                                    break;
                            }
                        }
                    }
                }
            }
        }

        if(Input.GetKeyDown(KeyCode.LeftShift) ||
            Input.GetKeyDown(KeyCode.RightShift))
        {
            if(currentCamera != null)
            {
                switch (cameraView)
                {
                    case CameraView.Z:
                        break;
                    case CameraView.A:
                        if (aCamChannels.Contains(currentScreen))
                        {
                            if (aCamChannels.IndexOf(currentScreen) < aCamChannels.Count)
                            {
                                currentScreen = aCamChannels[aCamChannels.IndexOf(currentScreen) + 1];
                            }
                            else
                            {
                                currentScreen = aCamChannels[0];
                            }
                        }
                        break;
                    case CameraView.B:
                        if (bCamChannels.Contains(currentScreen))
                        {
                            if (bCamChannels.IndexOf(currentScreen) < bCamChannels.Count)
                            {
                                currentScreen = bCamChannels[bCamChannels.IndexOf(currentScreen) + 1];
                            }
                            else
                            {
                                currentScreen = bCamChannels[0];
                            }
                        }
                        break;
                    case CameraView.C:
                        if (cCamChannels.Contains(currentScreen))
                        {
                            if (cCamChannels.IndexOf(currentScreen) < cCamChannels.Count)
                            {
                                currentScreen = cCamChannels[cCamChannels.IndexOf(currentScreen) + 1];
                            }
                            else
                            {
                                currentScreen = cCamChannels[0];
                            }
                        }
                        break;
                    case CameraView.D:
                        if (dCamChannels.Contains(currentScreen))
                        {
                            if (dCamChannels.IndexOf(currentScreen) < dCamChannels.Count)
                            {
                                currentScreen = dCamChannels[dCamChannels.IndexOf(currentScreen) + 1];
                            }
                            else
                            {
                                currentScreen = dCamChannels[0];
                            }
                        }
                        break;
                    default:
                        Debug.LogError("POV (" + gameObject.name + "): No such camera exists.");
                        break;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentCamera = null;
            cameraView = CameraView.Z;
            ChangeView(cameraView);
        }
    }

    public static void ChangeView(CameraView view)
    {
        switch (view)
        {
            default:
                break;
        }
    }
    */
}
