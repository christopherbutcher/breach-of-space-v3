using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//connect to robot movement script once Chris is done importing the level that Kyle created
public class ResourceCount : MonoBehaviour
{
    private int resourcesAmount; //amount of resources
    private Text resourceText; //resource Text

    Material robotResource;
    // Start is called before the first frame update

    void Start()
    {
        //playr starts with 0 resources
        resourcesAmount = 0;
        SetresourceText();
    }

    // Update is called once per frame
    void OnDestroy()
    {
        //collects the resource that was made; destroys it when picked up 
        Destroy(robotResource);
    }

    //when the Robot walks over/towards/collects a resource the resource count will go up by 1
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Resource")
        {
            other.gameObject.SetActive(false);
            resourcesAmount += 1;
            SetresourceText();
        }
    }

    //Text for the resource counter
    void SetresourceText()
    {
        resourceText.text = "Resources:" + resourcesAmount.ToString();
    }
}
