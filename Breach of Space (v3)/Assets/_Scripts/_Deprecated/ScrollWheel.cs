using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ScrollWheel : MonoBehaviour
{
    CinemachineVirtualCamera cvCam;
    int startingView = 60;
    public static bool changingViews;
    //works with updated scroll wheel Vector3
    // public zoomSpeed;
    // public float minZoomDist;
    // public float maxZoomDist;
    public static List<CinemachineVirtualCamera> allCams = new List<CinemachineVirtualCamera>();


    //This script allows the player to 
    // Start is called before the first frame update
    void Start()
    {
        cvCam = GetComponent<CinemachineVirtualCamera>();
        cvCam.m_Lens.FieldOfView = startingView;
        foreach(CinemachineVirtualCamera v in FindObjectsOfType<CinemachineVirtualCamera>())
        {
            allCams.Add(v);
        }
    }

    // Update is called once per frame
    void Update()
    {

        //Scroll Wheel using Vector3
        //Just needs to be changed to work with CinemaMachine
        //float scrollInput = scrollInput.GetAxis("Mouse ScrollWheel");
       // float dist = Vector3.Distance(transform.position, cam.transform.position);

      /// if (dist < minZoomDist && scrollInput > 0.0f)
        //    return;
       // else if (dist > maxZoomDist && scrollInput < 0.0f)
        //    return;

       // cam.transform.position += cam.tramsform.forward * scrollInput * zoomSpeed;
   // }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (cvCam.m_Lens.FieldOfView > 25)
            {
                cvCam.m_Lens.FieldOfView--;
            }

            //GetComponent<Camera>().fieldOfView--;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (cvCam.m_Lens.FieldOfView < 100)
            {
                cvCam.m_Lens.FieldOfView++;
              
            }
            //GetComponent<Camera>().fieldOfView++;
        }

        if (changingViews)
        {
            // This is kind of a hack??? - c
            foreach(CinemachineVirtualCamera v in allCams)
            {
                if(v.m_Lens.FieldOfView != startingView)
                {
                    v.m_Lens.FieldOfView = startingView;
                }
            }
            changingViews = false;
        }

        

    }
}
