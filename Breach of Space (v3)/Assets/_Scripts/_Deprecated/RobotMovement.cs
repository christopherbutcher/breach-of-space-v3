using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Panda;

public class RobotMovement : MonoBehaviour
{
    /*
    public NavMeshAgent navAgent;       // NavMeshAgent of the Robot.
    public Animator anim;               // Animator of the Robot.

    public Vector3 destination;         // The goal location of the Robot.

    public float speed;                 // Current speed of the Robot.
    static float REG_SPEED = 3.5f;      // Standard speed of all Robots.

    protected bool working;                       // True if Robot is working.
    protected bool walking;                       // True if Robot is moving.

    // private int totalTask;

    //Taken from the Basic Worker Agent
    public PlayerMove player;
    bool playerAware = false;
    bool canSee = false;

    public RobotStates curState = new RobotStates();

    public enum RobotStates
    {
        Idle,
        HeardSomething,
        Chasing,
        Searching,
        Walking,
        Working, 
        Trapped,
        Dead,
        PoweredDown
    };

    // Start is called before the first frame update
    public virtual void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        speed = REG_SPEED;

        SetRobotDestination(transform.position);
        curState = RobotStates.Idle;
    }

    // Update is called once per frame
    public virtual void Update()
    {
        // Sets the animations.
        anim.SetBool("isWalking", walking);
        anim.SetBool("isWorking", working);


        if (Vector3.Distance(transform.position, destination) > navAgent.stoppingDistance)
        {
            //Debug.Log("%%" + transform.position + ":" + destination);
            //Debug.Log("%%" + name+":"+Vector3.Distance(transform.position, destination));
            walking = true;
        }
        else
        {
            walking = false;

            if (!working)
            {
                
            }
            else
            {

            }
        }

        if (canSee)
        {
            if (LineofSight())
            {
                if (!playerAware)
                {
                    playerAware = true;
                    player.awareUp();
                }
                /*if (player.sus && catchTimer <= 0)
                {
                    player.Caught();
                    catchTimer += 3.5f;
                }*/ /*
            }
            else
            {
                if (playerAware)
                {
                    playerAware = false;
                    player.awareDown();
                }
            }
        }

    }

    public void SetRobotDestination(Vector3 v)
    {
        working = false;
        NavMesh.SamplePosition(v, out NavMeshHit hit, 10, 1);
        destination = new Vector3(v.x, hit.position.y, v.z);
        navAgent.SetDestination(destination);
        //Debug.LogError("Robot going to " + destination);
    }

    public void SetRobotWorking(bool answer, Vector3 target)
    {
        Debug.Log("Set Robot Working: " + answer + ", " + target);
        working = answer;

        if (answer)
        {
            SetRobotDestination(transform.position);
        }
        else
        {
            SetRobotDestination(transform.position);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player") && LineofSight())
        {
            canSee = true;
            AudioHandler.PlaySound("event:/s_Seen", this.transform.position);
        }
        if (collider.CompareTag("SABO"))
        {
            if (collider.name != "New Monitor")
            {
                /*
                if (collider.GetComponent<OHInteractable>().Tampered)<------------------hi chris
                {
                    working = true;
                    curState = RobotStates.Working;
                }
                */ /*
            }
        }
    }

    private bool LineofSight()
    {
        RaycastHit hit;
        var rayDirection = player.transform.position - transform.position;
        if (Physics.Raycast(transform.position, rayDirection, out hit, Mathf.Infinity))
        {
            if (hit.transform.tag == "Player")
            {
                //Debug.Log("Can see player");
                //AudioHandler.PlaySound("event:/s_Seen", this.transform.position);
                return true;
            }
            else
            {
                //Debug.Log("Can not see player");
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canSee = false;
            if (playerAware)
            {
                playerAware = false;
                player.awareDown();
            }
        }
    }*/
}
