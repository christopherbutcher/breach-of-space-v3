using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;
using Cinemachine;
using FMODUnity;
using UnityEngine.UI;

// 2021.10.05 @ 03:11 PM - CB.
public class PlayerMove : MonoBehaviour
{
    public float idletime; //idle time for attract trailer scene 
    public float easingDuration = 0.3f;
    public bool isChasing = false; //whether or not any robot is chasing the player
    private bool callOnceForVignette = false;
    public const float ogFOV = 60f;
    public GameObject vignetteEffect;
    public GameObject CMvcam1;

    public CharacterController controller;

    public Transform cam; //the camera's Transform
    public CameraController camController;
    public Transform playerBody; //the transform of the capsule the represents the player

   private FMODUnity.StudioEventEmitter footstepEmitter;

    //private NInventory inventoryL;
    //private NInventory inventoryR;

    //public PicckUp pickupable = null;
    
    Vector3 velocity; //player's velocity

    public float mass = 3.0f; //player's mass, used for calculations in movement
    public static float mouseSen = 200f;
    public float playerSpeed = 7.0f;

    public float playerJumpForce = 4f;
    private float gravVal = -9.8f;

    public float raycastDistance = 0.56f; //distance of the raycast to check if player is touching the ground.
    public float capRaycastDistance = 0.55f; //distance of the raycast to check if the player can uncrouch.

    //public HandsUI handL;
    //public HandsUI handR;
    //public NItem item;

    public bool active = true; //what is this? -JS
    public bool addTex = false; //what is this? -JS
    public bool checking = false; //what is this? -JS

    public bool Grounded = true;

    public EndUI end;
    public GameObject mouseIcon; //Popup used to show the player can click on a given portion
    public int playerBolt = 0; //Can this be removed @Ali? -JS
    public int playerNuts = 0; //Can this be removed @Ali? -JS
    public int playerMetal = 0; //Can this be removed @Ali? -JS

    private bool isPushBack = false; //boolean for when forcefield pushes away player
    public bool scrapIsOpen = false; //Can this be removed @Ali? - JS
    public bool craftIsOpen = false; //Can this be removed @ali? -JS
    public ScrapCraft kraft; //Can this be removed @Ali? -JS
    public GameObject scrapMenu; //Can this be removed @Ali? -JS
    public GameObject craftMenu; //Can this be removed @Ali? -JS
    //public GameObject noiseMake;
    public Transform currPlayPos; // Constantly gives an updated player position


    public DialogueTxt dTxxt; //Can this be removed @Ali? -JS (believe this is text that pops up on picking up scrap)

    public PileBolts nearBolt = null; //Can this be removed @Ali? -JS
    public PileNuts nearNut = null; //Can this be removed @Ali? -JS
    public PileMetel nearMetal = null; //Can this be removed @Ali? -JS

    public Vector3 CrouchScale = new Vector3(0.47f, 0.15f, 0.47f); //the scale ratio for the player when they're crouching

    public Vector3 CrouchCorrection = new Vector3(0f, 0.4f, 0f); //the force applied to the player to make sure they don't clip into the ground when they uncrouch.
    public Vector3 FullScale = new Vector3(0.47f, 0.47f, 0.47f); //the scale ratio for the player when they're not crouching.
    public Vector3 currentVelocity = Vector3.zero; //velocity of the player used in adjustment of player calculation.
    private Vector3 impact = Vector3.zero; //@MarticZtn can you describe this? -JS

    private Collider forceFieldCollider;
    public alphaSpammer fadeOut; //a way to fade out the player's vision by slowly loading in a black image.
    public static bool gameOver; //set to true when player game overs
    private bool inMinigame = false; //tracks whether or not player is in sabotage minigame
    private int movementState = 0;  //  0:  IDLE
                                    //  1:  CROUCHING
                                    //  2:  WALKING
                                    //  3:  RUNNING

    private void Start()
    {
  
        gameOver = false;
        footstepEmitter = GetComponent<FMODUnity.StudioEventEmitter>();
        footstepEmitter.Play();
        footstepEmitter.SetParameter("MovementState", 0);
    }

    private void Awake() 
    {
        currPlayPos = this.transform;
        //kraft = GetComponent<ScrapCraft>(); @ali what do we still need here? -JS
        //inventoryL = new NInventory();
        //inventoryR = new NInventory();
        //scrapMenu.SetActive(false); <----------------------------- turned off to check something -c.
        //craftMenu.SetActive(false); <-----------------------------------------------------
    }

    public void Caught()
    {
        end.GameOver(); 
    }


    private void PlayerPushBack(float deltaTime)
    {
        gameObject.GetComponent<CharacterController>().Move(impact * deltaTime);
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * deltaTime);
        Debug.Log("Player got pushed back by FoeceField");
    }

    IEnumerator StopCheatingRobot(float time)
    {
        yield return new WaitForSeconds(time);
        print("robot stop cheating");
        Robot.cheating = false;
        Robot robot = RobotBlackboard.GetNearestRobot(transform.position);
        RobotBlackboard.ReportSuspiciousActivity(robot, robot.transform.position);
        
    }
    //DELETE WHEN CHEAT IS NO LONGER NEEDED
    private int cheat = 0;
    private int audioR = 0;
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("ForceField"))
        {
            Vector3 direction = collider.gameObject.transform.position - transform.position;
            direction = -direction.normalized;
            AddImpact(direction, 40f);
            isPushBack = true;
        }

        if (collider.CompareTag("cheat") && cheat < 1)
        {
            Robot.cheating = true;
            StartCoroutine(StopCheatingRobot(10f));
            cheat += 1;
            //AudioHandler.PlaySound("event:/s_Seen", this.transform.position);
            AudioHandler.chaos = 1;
        }

        if (collider.CompareTag("gdexWin"))
        {
            fadeOut.alphaFill();
        }

        if (collider.CompareTag("audioTick")&& audioR < 1)
        {
            AudioHandler.chaos = 0.2f;
            audioR += 1;
        }

        if (collider.CompareTag("SABO"))
        {
            //nearby = collider.GetComponent<OHInteractable>();
        }


        if (collider.CompareTag("Bolt"))
        {
            nearBolt = collider.GetComponent<PileBolts>();
            dTxxt.TriggerScrap(pileB:nearBolt);
        }
/*
        if (collider.CompareTag("Nut"))
        {
            nearNut = collider.GetComponent<PileNuts>();
            dTxxt.TriggerScrap(pileN:nearNut);
        }

        if (collider.CompareTag("Metal"))
        {
            nearMetal = collider.GetComponent<PileMetel>();
            dTxxt.TriggerScrap(pileM:nearMetal);
        }*/
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ForceField"))
        {
            isPushBack = false;
        }

        if (other.CompareTag("SABO"))
        {
            //nearby = null;
        }

        if (other.CompareTag("Bolt"))
        {
            nearBolt = null;
        }

        if (other.CompareTag("Nut"))
        {
            nearNut = null;
        }

        if (other.CompareTag("Metal"))
        {
            nearMetal = null;
        }
    }

    bool IsGrounded()
    {
        RaycastHit hit;
        int mask = 1 << LayerMask.NameToLayer("Ground");

        //Raycast downwards
        if (Physics.Raycast(transform.position, Vector3.down, out hit,
            raycastDistance, mask))
        {
            return true;
        }
        return false;
    }

    bool IsCapped()
    {
        RaycastHit hit;

        //Raycast downwards
        if (Physics.Raycast(transform.position, Vector3.up, out hit,
            capRaycastDistance))
        {
            return true;
        }
        return false;
    }



    public void ButtonMouseLock()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void ButtonMouseUnlock()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }


    public void ThrowItem()
    {
        //noiseMake.transform.position = currPlayPos.transform.position;
        kraft.itemInUse = false;
    }

    public void AddImpact(Vector3 direction, float force)
    {
        direction.Normalize();

        if (direction.y < 0)
            direction.y = -direction.y;

        impact += direction.normalized * force / mass;
    }

    public void enableStealthMode(bool isEnabled, float deltaTime)
    {
        // Log info
        if (vignetteEffect == null)
            Debug.Log("vignetteEffect GameObject is null");

        // Set log string
        Debug.Log(isEnabled ? "Stealth mode..." : "You are being chased...");

        // Set vignette effect
        Vignette vignetteComp = null;
        Vignette tmp;

        // Try to get the override component in Volume
        Volume volume = vignetteEffect.GetComponent<Volume>();
        if (volume.profile.TryGet<Vignette>(out tmp))
            vignetteComp = tmp;

        GameObject cameraObject = gameObject.transform.GetChild(0).gameObject;
        CinemachineVirtualCamera vcam = CMvcam1.GetComponent<CinemachineVirtualCamera>();
        if (isEnabled)
        {
            StartCoroutine(startVignetteEasing(vignetteComp, vignetteComp.intensity.value, 0.4f));
            StartCoroutine(startFOVEasing(vcam, vcam.m_Lens.FieldOfView, ogFOV == vcam.m_Lens.FieldOfView ? vcam.m_Lens.FieldOfView : vcam.m_Lens.FieldOfView - 20));

            // Disable camera shake
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0;
        }
        else
        {
            StartCoroutine(startVignetteEasing(vignetteComp, vignetteComp.intensity.value, 0f));
            StartCoroutine(startFOVEasing(vcam, vcam.m_Lens.FieldOfView, vcam.m_Lens.FieldOfView + 20));

            // Enable camera
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1.5f;
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 2;
        }
    }

    // Easing animation function for the change of FOV
    public IEnumerator startFOVEasing(CinemachineVirtualCamera vcam, float startVal, float endVal)
    {
        // Total time elapsed
        float timeElapsed = 0;

        // While the total time elapsed is smaller than designated duration
        // Start the easing animation
        while (timeElapsed < easingDuration)
        {
            // Easing function
            float t = timeElapsed / easingDuration;
            t = t * t * (3f - 2f * t);

            vcam.m_Lens.FieldOfView = Mathf.Lerp(startVal, endVal, t);
            timeElapsed += Time.deltaTime;

            yield return null;
        }

        // Apply value to camera FOV
        vcam.m_Lens.FieldOfView = endVal;
    }

    // Easing animation function for the vignette effect
    public IEnumerator startVignetteEasing(Vignette vignette, float startVal, float endVal)
    {
        float timeElapsed = 0;

        while (timeElapsed < easingDuration)
        {
            float t = timeElapsed / easingDuration;
            t = t * t * (3f - 2f * t);

            // Set the intensity of the vignette effect
            if (vignette != null)
                vignette.intensity.value = Mathf.Lerp(startVal, endVal, t);

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        vignette.intensity.value = endVal;
    }

    void Update()
    {
        if (camController.getsaboItem() != null && !inMinigame)
        {
            mouseIcon.SetActive(true);
        }
        else
        {
            mouseIcon.SetActive(false);
        }

        // If the player is being chased and call once is false
        if (isChasing && callOnceForVignette)
        {
            callOnceForVignette = false;
            enableStealthMode(false, Time.deltaTime);
        }

        // If the player is not being chased and call once is false
        if (!isChasing && !callOnceForVignette)
        {
            callOnceForVignette = true;
            enableStealthMode(true, Time.deltaTime);
        }

        // Debug.Log(impact.magnitude);
        if (impact.magnitude > 0.2f)
            PlayerPushBack(Time.deltaTime);

        

        //currPlayPos.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 1);
        // This code is for preparing the noisemaker throw by putting a noisemaker behind the player
        /*if (kraft.itemInUse == true && Input.GetKeyDown("b"))
        {
            ThrowItem();
            Debug.LogWarning(currPlayPos.transform.position);
        }--------------------------------------------------------------------------------------------------------------------------------------------
        */
        //if (checking)
        //{
        //player can't do anything other than click buttons
        //}
        //Ltran.texture = objImg.tex;
        /*if (checking)
        {
         direction = new Vector3(0f, 0f, 0f).normalized;
        }
        if(direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            controller.Move(moveDir.normalized * playerSpeed * Time.deltaTime);
        }*/

        if(!inMinigame)
        {

            // Player movement
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            if (/*Grounded &&*/ (x != 0 || z != 0))
            {
                if (Input.GetButton("Crouch"))
                {
                    movementState = 1;
                }
                else
                {
                    movementState = 2;
                }
                
            }
            else
            {
                movementState = 0;
            }
            footstepEmitter.SetParameter("MovementState", movementState);
            if (Grounded)
            {
                if (Input.GetButtonDown("Jump"))
                {
                    velocity.y += Mathf.Sqrt(playerJumpForce * -3.0f * gravVal);
                }
            }
            if (Input.GetButton("Crouch"))
            {
                playerBody.localScale = CrouchScale;
                raycastDistance = 0.33f;
                playerSpeed = 3f;
            }
            else if (raycastDistance < 0.55f && !IsCapped())
            {
                playerBody.localScale = FullScale;
                velocity.y += 20f;
                controller.Move(velocity * Time.deltaTime);
                velocity.y -= 20f;
                raycastDistance = 0.55f;
                playerSpeed = 7f;
            }
            Vector3 move = transform.right * x + transform.forward * z;
            currentVelocity = move + velocity;
            controller.Move(move * playerSpeed * Time.deltaTime);

            // Player movement speed
            controller.Move(velocity * Time.deltaTime);
            Grounded = IsGrounded();
            if (!Grounded)
            {
                velocity.y += gravVal * Time.deltaTime;
            }
            else
            {
                velocity.y = 0;
            }

            if (Input.GetMouseButtonDown(0) /*&& nearby != null*/)
            {
                print("mouseclick");
                if(camController.getsaboItem() != null && !inMinigame)
                {
                    print("entered if");
                    inMinigame =true;
                    camController.flipLock();
                    camController.saboItem.GetComponent<Sabotage>().AttemptSabo();
                }
                else if (inMinigame)
                {
                    inMinigame = false;
                    camController.flipLock();
                }
                checking = true;
                addTex = false;
                active = false;
                Invoke("NotSus", 3.0f);
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0)/*&& nearby != null*/)
                {
                    inMinigame = false;
                    camController.flipLock();
                }
            if (camController.caught)
            {
                camController.saboItem.GetComponent<SabotageMinigame>().Failure();
            }
        }

        if (Input.GetKeyDown("u"))
        {
            ButtonMouseUnlock();
        }

        if (Input.GetKey("r") && (Input.GetKey("left alt") || Input.GetKey("right alt")))
        {
           Scene scene = SceneManager.GetActiveScene(); 
           SceneManager.LoadScene(scene.name);
           AudioHandler.FadeOutLevelMusic();
        }

        /*if (Input.GetKeyDown("o") && scrapIsOpen == false && craftIsOpen == false)
        {
            // Open the scrap menu
            Debug.LogWarning("Scrap Menu Open");
            scrapMenu.SetActive(true);
            scrapIsOpen = true;
        }
        else if (Input.GetKeyDown("o") && scrapIsOpen == true)
        {
            // Close the scrap menu
            Debug.LogWarning("Scrap Menu Closed");
            scrapMenu.SetActive(false);
            scrapIsOpen = false;
        }*/

        // Below is the code for the opening and crafting scrap
        // P = Open Menu, O = Craft Noisemaker, L = Scrap Crafted Item
        if (Input.GetKeyDown("p"))
        {
            Debug.LogWarning("Craft Menu Open");
        }
        if (Input.GetKey("p")/* && craftIsOpen == false && scrapIsOpen == false*/)
        {
            // Open the craft menu
            // Debug.LogWarning("Craft Menu Open");
            craftMenu.SetActive(true);
            //craftIsOpen = true;
            if (Input.GetKeyUp("o"))
            {
                kraft.DoCraft();
                craftMenu.SetActive(false);
            }
            if (Input.GetKeyUp("l"))
            {
                kraft.UndoCraft();
                craftMenu.SetActive(false);
            }
        }
        if (Input.GetKeyUp("p")/* && craftIsOpen == true*/)
        {
            // Close the craft menu
            Debug.LogWarning("Craft Menu Closed");
            craftMenu.SetActive(false);
            dTxxt.ClearTxt();
            //craftIsOpen = false;
        }
        else
        {
            //ButtonMouseLock();
        }

    }

// 09/30/21 - Idle time, swithces to attract trailer scene after 30 seconds of no input

    private void FixedUpdate()
    { 


        if (!Input.anyKey)
        {
            idletime = idletime + 1;
        }

        else
        {
            idletime = 0;
        }

           if (idletime == 3500)
        {
            AudioHandler.FadeOutLevelMusic();
            Debug.Log("No input after 30 seconds");
            SceneManager.LoadScene("AttractTrailer");
        }
    }
}
