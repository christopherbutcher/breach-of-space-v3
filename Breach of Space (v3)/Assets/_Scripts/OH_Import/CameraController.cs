﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//LAST EDITED: 9/28/21 @ 10:48 by JS
public class CameraController : MonoBehaviour
{
    public float turnSpeed = 4.0f;
    public float verticalSen = 150f;
    public float horizontalSen = 200;

    private float minTilt = -90f;
    private float maxTilt = 90f;
    private float rotationX = 0f;
    private bool lockCamera = false;

    public Transform player;

    private Vector3 offset;
    private GameObject cam;
    private float raycastDistance = 7f;
    public GameObject saboItem = null;
    public GameObject resetPop;
    public bool caught = false;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        offset = new Vector3(player.position.x, player.position.y + 3.0f, player.position.z + 4.0f);
        cam = gameObject;
    }

    void LateUpdate()
    {
        //disabled pause menu for now.
        /*if (PauseMenu.GameIsPaused == false)
        {
            offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
            transform.position = player.position + offset;
            transform.LookAt(player.position);
        }*/
    }

    public void flipLock()
    {
        lockCamera = !lockCamera;
    }

    private void Update()
    {
        if (caught)
        {
            float xr = 0f;
            float yr = 0f;
            float zr = 0f;
            float xp = 0f;
            float yp = 0f;
            float zp = 0f;
            if (cam.transform.localRotation.x != 0)
            {
               if (cam.transform.localRotation.x > 0)
               {
                   xr = -5f * Time.deltaTime;
                   if(cam.transform.localRotation.x + xr < 0)
                   {
                       xr = 0 - cam.transform.localRotation.x;
                   }
               }
               else if (cam.transform.localRotation.x < 0)
               {
                   xr = 5f * Time.deltaTime;
                   if(cam.transform.localRotation.x + xr > 0)
                   {
                       xr = 0 - cam.transform.localRotation.x;
                   }
               }
            }
            if (cam.transform.localRotation.y != 180)
            {
               if (cam.transform.localRotation.y > 180)
               {
                   yr = -5f * Time.deltaTime;
                   if(cam.transform.localRotation.y + yr < 180)
                   {
                       yr = 180 - cam.transform.localRotation.y;
                   }
               }
               else if (cam.transform.localRotation.y < 180)
               {
                   yr = 5f * Time.deltaTime;
                   if(cam.transform.localRotation.y + yr > 180)
                   {
                       yr = 180 - cam.transform.localRotation.y;
                   }
               }
            }
            if (cam.transform.localRotation.z != 0)
            {
               if (cam.transform.localRotation.z > 0)
               {
                   zr = -5f * Time.deltaTime;
                   if(cam.transform.localRotation.z + zr < 0)
                   {
                       zr = 0 - cam.transform.localRotation.z;
                   }
               }
               else if (cam.transform.localRotation.z < 0)
               {
                   zr = 5f * Time.deltaTime;
                   if(cam.transform.localRotation.z + zr > 0)
                   {
                       zr = 0 - cam.transform.localRotation.z;
                   }
               }
            }
            cam.transform.localRotation = Quaternion.Euler((cam.transform.localRotation.x + xr),180,(cam.transform.localRotation.z + zr));
            //location ----------------------------------------
            if (cam.transform.localPosition.x != 0)
            {
               if (cam.transform.localPosition.x > 0)
               {
                   xp = -5f * Time.deltaTime;
                   if(cam.transform.localPosition.x + xp < 0)
                   {
                       xp = 0 - cam.transform.localPosition.x;
                   }
               }
               else if (cam.transform.localPosition.x < 0)
               {
                   xp = 5f * Time.deltaTime;
                   if(cam.transform.localPosition.x + xp > 0)
                   {
                       xp = 0 - cam.transform.localPosition.x;
                   }
               }
            }
            if (cam.transform.localPosition.y != 3.5f)
            {
               if (cam.transform.localPosition.y > 3.5f)
               {
                   yp = -5f * Time.deltaTime;
                   if(cam.transform.localPosition.y + yp < 3.5f)
                   {
                       yp = 3.5f - cam.transform.localPosition.y;
                   }
               }
               else if (cam.transform.localPosition.y < 3.5f)
               {
                   yp = 5f * Time.deltaTime;
                   if(cam.transform.localPosition.y + yp > 3.5f)
                   {
                       yp = 3.5f - cam.transform.localPosition.y;
                   }
               }
            }
            if (cam.transform.localPosition.z != 3f)
            {
               if (cam.transform.localPosition.z > 3f)
               {
                   zp = -5f * Time.deltaTime;
                   if(cam.transform.localPosition.z + zp < 3f)
                   {
                       zp = 3f - cam.transform.localPosition.z;
                   }
               }
               else if (cam.transform.localPosition.z < 3f)
               {
                   zp = 5f * Time.deltaTime;
                   if(cam.transform.localPosition.z + zp > 3f)
                   {
                       zp = 3f - cam.transform.localPosition.z;
                   }
               }
            }
            cam.transform.localPosition += new Vector3(xp,yp,zp);
        }
        else if (!lockCamera)
        {
            // Player camera tilt (look up and down)
            float mouseX = Input.GetAxis("Mouse X") * horizontalSen * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * verticalSen * Time.deltaTime;

            rotationX -= mouseY;
            rotationX = Mathf.Clamp(rotationX, minTilt, maxTilt);

            transform.localRotation = Quaternion.Euler(rotationX, 0f, 0f);
            player.Rotate(Vector3.up * mouseX);

            RaycastHit hit;
            int mask = 1 << LayerMask.NameToLayer("Sabotagable");

            //  Raycast forwards
            Debug.DrawRay(transform.position, transform.forward, Color.cyan);
            if (Physics.Raycast(transform.position, transform.forward, out hit,
                raycastDistance, mask))
            {
                saboItem = hit.collider.gameObject;
                //  highlight saboItem
                highlightObject(saboItem);
                Debug.Log(saboItem.name);
            }
            else
            {
                removeHighlights(saboItem);
                saboItem = null;
            }
        }
    }

    void highlightObject(GameObject targetObject)
    {

    }

    void removeHighlights(GameObject targetObject)
    {

    }

    public GameObject getsaboItem()
    {
        return saboItem;
    }

    public void gotCaught(GameObject robo)
    {
        cam.transform.parent = robo.transform;
        caught = true;
        StartCoroutine(resetPopup(4f));
    }

    IEnumerator resetPopup(float time)
    {
        yield return new WaitForSeconds(time);
        resetPop.SetActive(true);
    }
}
